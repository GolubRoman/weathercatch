//
//  Weather.swift
//  WeatherCatch
//
//  Created by Roman Holub on 10/23/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation

class Weather {
    var locationName, weatherStatus, weatherDescription, icon: String?
    var lat, lon, temp, windSpeed, seaLevel, groundLevel: Double?
    var pressure, humidity, cloudiness, windDegree, timezone, tempMin, tempMax: Int?
    var sunrise, sunset: Int64?
    
    init(locationName: String?, weatherStatus: String?, weatherDescription: String?, icon: String?,
         lat: Double?, lon: Double?, temp: Double?, tempMin: Int?, tempMax: Int?,
         windSpeed: Double?, windDegree: Int?, seaLevel: Double?, groundLevel: Double?,
         pressure: Int?, humidity: Int?, cloudiness: Int?, sunrise: Int64?, sunset: Int64?, timezone: Int?) {
             self.locationName = locationName
             self.weatherStatus = weatherStatus
             self.weatherDescription = weatherDescription
             self.icon = icon
             self.lat = lat
             self.lon = lon
             self.temp = temp
             self.tempMin = tempMin
             self.tempMax = tempMax
             self.windSpeed = windSpeed
             self.windDegree = windDegree
             self.seaLevel = seaLevel
             self.groundLevel = groundLevel
             self.pressure = pressure
             self.humidity = humidity
             self.cloudiness = cloudiness
             self.sunrise = sunrise
             self.sunset = sunset
             self.timezone = timezone
    }
    
}
