//
//  MainViewController.swift
//  WeatherCatch
//
//  Created by Roman Holub on 10/21/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import UIKit
import Lottie
import RxSwift
import RxCocoa
import RxGesture
import RxAlamofire
import Alamofire

class CityWeatherViewController: UIViewController {

    @IBOutlet weak var backgroundView: UIImageView!
    @IBOutlet weak var statusAnimationView: AnimationView!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var statusNameLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var sunriseTimeLabel: UILabel!
    @IBOutlet weak var sunsetTimeLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var minTemperatureLabel: UILabel!
    @IBOutlet weak var maxTemperatureLabel: UILabel!
    @IBOutlet weak var seaLevelLabel: UILabel!
    @IBOutlet weak var groundLevelLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var windDegreeLabel: UILabel!
    
    let viewModel = CityWeatherViewModel()
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.insetsLayoutMarginsFromSafeArea = false
        viewModel.observeWeather()?
            .observeOn(MainScheduler.instance)
            .subscribe(onCompleted: {
                self.initViews()
            }, onError: { error in
                print("Error: ", error)
            }).disposed(by: disposeBag)
    }
    
    func setCity(city: City?) {
        viewModel.setCity(city: city)
    }
}
