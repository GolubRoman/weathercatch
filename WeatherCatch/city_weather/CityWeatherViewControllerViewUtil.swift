//
//  MainViewControllerViewUtil.swift
//  WeatherCatch
//
//  Created by Roman Holub on 12/12/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import UIKit
import Lottie

extension CityWeatherViewController {

    func initViews() {
        backgroundView.image = viewModel.getWeatherBackground()
        setWeatherStatusAnimation(animation: viewModel.getWeatherStatusAnimation())
        cityNameLabel.text = viewModel.getCityNameText()
        statusNameLabel.text = viewModel.getWeatherDescriptionText()
        temperatureLabel.text = viewModel.getTemperatureText()
        sunriseTimeLabel.text = viewModel.getSunriseTimeText()
        sunsetTimeLabel.text = viewModel.getSunsetTimeText()
        pressureLabel.text = viewModel.getPressureText()
        humidityLabel.text = viewModel.getHumidyText()
        minTemperatureLabel.text = viewModel.getMinTemperatureText()
        maxTemperatureLabel.text = viewModel.getMaxTemperatureText()
        seaLevelLabel.text = viewModel.getSeaLevelText()
        groundLevelLabel.text = viewModel.getGroundLevelText()
        windSpeedLabel.text = viewModel.getWindSpeedText()
        windDegreeLabel.text = viewModel.getWindDegreeText()
    }
    
    private func setWeatherStatusAnimation(animation: Animation) {
        statusAnimationView.animation = animation
        statusAnimationView.loopMode = LottieLoopMode.loop
        statusAnimationView.play()
    }
}
