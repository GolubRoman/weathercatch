//
//  MainViewModel.swift
//  WeatherCatch
//
//  Created by Roman Holub on 10/21/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import RxSwift
import Lottie

class CityWeatherViewModel {
    
    private var weatherInteractor: WeatherInteractor?
    private var city: City?
    private var weather: Weather?

    init() {
        weatherInteractor = (UIApplication.shared.delegate as! AppDelegate).interactorComponent.provideWeatherInteractor()
    }
    
    func setCity(city: City?) {
        self.city = city
    }
    
    func observeWeather() -> Completable? {
        if let city = self.city {
            return weatherInteractor?.observeWeatherForCity(city: city)?
                .observeOn(MainScheduler.instance)
                .flatMap { weather in
                    return Maybe.create { maybe in
                        weather.locationName = city.name
                        self.weather = weather
                        maybe(.completed)
                        return Disposables.create()
                    }
                }.asObservable()
            .asCompletable()
        } else {
            return Completable.empty()
        }
    }
    
    func getSunriseTimeText() -> String {
        return getTimeByMillis(millis: weather?.sunrise)
    }
    
    func getSunsetTimeText() -> String {
        return getTimeByMillis(millis: weather?.sunset)
    }
    
    func getPressureText() -> String {
        return String(weather?.pressure ?? 0) + "hPa"
    }
    
    func getHumidyText() -> String {
        return String(weather?.humidity ?? 0) + "%"
    }
    
    func getMinTemperatureText() -> String {
        return getTemperature(temp: weather?.tempMin)
    }
    
    func getMaxTemperatureText() -> String {
        return getTemperature(temp: weather?.tempMax)
    }
    
    func getCurrentTemperatureText() -> String {
        return getTemperature(temp: Int(weather?.temp?.rounded() ?? 0))
    }
    
    func getSeaLevelText() -> String {
        return String(weather?.seaLevel ?? 1023.1) + "m"
    }
    
    func getGroundLevelText() -> String {
        return String(weather?.groundLevel ?? 1120.33) + "m"
    }
    
    func getWindSpeedText() -> String {
        return String(weather?.windSpeed ?? 0) + "mps"
    }
    
    func getWindDegreeText() -> String {
        return String(weather?.windDegree ?? 0) + "°"
    }
    
    func getTemperatureText() -> String {
        return getTemperature(temp: weather?.tempMin)
    }
    
    func getWeatherDescriptionText() -> String {
        return weather?.weatherDescription ?? ""
    }
    
    func getCityNameText() -> String {
        return weather?.locationName ?? ""
    }
    
    func getWeatherBackground() -> UIImage {
        if Array(weather?.icon ?? "01d")[2] == "d" {
            return UIImage(named: "day_background")!
        } else {
            return UIImage(named: "night_background")!
        }
    }
    
    func getWeatherStatusAnimation() -> Animation {
        switch weather?.icon {
            case "01d": return Animation.named("01d")!
            case "02d": return Animation.named("02d")!
            case "03d": return Animation.named("03d")!
            case "04d": return Animation.named("03d")!
            case "09d": return Animation.named("09d")!
            case "10d": return Animation.named("09d")!
            case "11d": return Animation.named("11d")!
            case "13d": return Animation.named("13d")!
            case "50d": return Animation.named("50d")!
            case "01n": return Animation.named("01n")!
            case "02n": return Animation.named("02n")!
            case "03n": return Animation.named("03d")!
            case "04n": return Animation.named("03d")!
            case "09n": return Animation.named("09n")!
            case "10n": return Animation.named("09n")!
            case "11n": return Animation.named("11n")!
            case "13n": return Animation.named("13n")!
            case "50n": return Animation.named("50d")!
            default: return Animation.named("01d")!
        }
    }
    
    private func getTimeByMillis(millis: Int64?) -> String {
        return DateUtil.getStringTimeBeginningOfTheDayFromMillis(millis: millis)
    }
    
    private func getTemperature(temp: Int?) -> String {
        return String(temp ?? 0) + "°C"
    }
    
}
