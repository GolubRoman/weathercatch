//
//  CityWeatherPagerViewUtil.swift
//  WeatherCatch
//
//  Created by Roman Holub on 12/18/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import UIKit

extension CityWeatherPagerViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = cityViewControllers.firstIndex(of: viewController as! CityWeatherViewController)
        else { return nil }
        
        let previousIndex = viewControllerIndex - 1
        guard previousIndex >= 0 else { return nil }
        guard cityViewControllers.count > previousIndex else { return nil }
        
        return cityViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = cityViewControllers.firstIndex(of: viewController as! CityWeatherViewController)
        else { return nil }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = cityViewControllers.count
        guard orderedViewControllersCount != nextIndex else { return nil }
        guard orderedViewControllersCount > nextIndex else { return nil }
        
        return cityViewControllers[nextIndex]
    }
    
}
