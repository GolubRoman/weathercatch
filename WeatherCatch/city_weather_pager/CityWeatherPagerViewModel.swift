//
//  CityWeatherPagerViewModel.swift
//  WeatherCatch
//
//  Created by Roman Holub on 12/18/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import RxSwift

class CityWeatherPagerViewModel {
    
    private var cityInteractor: CityInteractor?

    init() {
        cityInteractor = (UIApplication.shared.delegate as! AppDelegate).interactorComponent.provideCityInteractor()
    }
    
    func observeSavedCities() -> Single<Array<City>>? {
        return cityInteractor?.observeSavedCities()
    }

}
