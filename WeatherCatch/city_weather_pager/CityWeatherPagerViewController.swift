//
//  CityWeatherPagerViewController.swift
//  WeatherCatch
//
//  Created by Roman Holub on 12/18/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class CityWeatherPagerViewController: UIPageViewController {
    
    private let disposeBag = DisposeBag()
    let viewModel = CityWeatherPagerViewModel()
    var cityViewControllers = Array<UIViewController>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self as UIPageViewControllerDataSource
        observeCities()
    }
    
    private func observeCities() {
        viewModel.observeSavedCities()?
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { cities in
                self.processUpcomingCities(cities: cities)
                self.setViewControllers([self.cityViewControllers[0]], direction: .forward, animated: true, completion: nil)
            }, onError: { error in print("Error: ", error) })
            .disposed(by: disposeBag)
    }
    
    private func processUpcomingCities(cities: Array<City>) {
        self.cityViewControllers.removeAll()
        cities.forEach {
            if let viewController = self.mapCityToCityWeatherViewController(city: $0) {
                self.cityViewControllers.append(viewController)
            }
        }
    }
    
    private func mapCityToCityWeatherViewController(city: City) -> UIViewController? {
        let viewController = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: "cityWeatherViewController") as! CityWeatherViewController
        viewController.setCity(city: city)
        return viewController
    }
}
