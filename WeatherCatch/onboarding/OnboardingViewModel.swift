//
//  OnboardingViewModel.swift
//  WeatherCatch
//
//  Created by Roman Holub on 9/30/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import RxSwift
import paper_onboarding

class OnboardingViewModel {
    
    private var itemArray = Array<OnboardingItemInfo>()
    private var cityInteractor: CityInteractor?
    
    init() {
        (UIApplication.shared.delegate as! AppDelegate).initDependencies()
        initOnboardingArray()
        cityInteractor = (UIApplication.shared.delegate as! AppDelegate).interactorComponent.provideCityInteractor()
    }
    
    func observeIfWeAlreadyHaveCities() -> Single<Bool>? {
        return cityInteractor?.observeSavedCities()?
            .map { cities in cities.count >= 1 }
    }
    
    func getOnboardingArray() -> Array<OnboardingItemInfo> {
        return itemArray
    }
    
    private func initOnboardingArray() {
        itemArray.append(OnboardingItemInfo(informationImage: UIImage(named: "helpful_app")!,
                                            title: "WeatherCatch's your good helper",
                                            description: "WeatherCatch is an app, that always provide you with detailed information about todays weather, allows to save cities and even days",
                                            pageIcon: (UIImage(named: "helpful_app_simple")!.withAlignmentRectInsets(UIEdgeInsets(top: -10, left: -10, bottom: -10, right: -10))),
                                            color: UIColor(red: 0.09, green: 0.71, blue: 0.46, alpha: 1),
                                            titleColor: .white,
                                            descriptionColor: .white,
                                            titleFont: .boldSystemFont(ofSize: 16),
                                            descriptionFont: .systemFont(ofSize: 14)))
        itemArray.append(OnboardingItemInfo(informationImage: UIImage(named: "city_save")!,
                                            title: "Save the cities",
                                            description: "Take advantage of being able to save any city you interested in to be aware of todays weather there",
                                            pageIcon: UIImage(named: "city_save_simple")!.withAlignmentRectInsets(UIEdgeInsets(top: -10, left: -10, bottom: -10, right: -10)),
                                            color: UIColor(red: 0.36, green: 0.32, blue: 0.88, alpha: 1),
                                            titleColor: .white,
                                            descriptionColor: .white,
                                            titleFont: .boldSystemFont(ofSize: 16),
                                            descriptionFont: .systemFont(ofSize: 14)))
        itemArray.append(OnboardingItemInfo(informationImage: UIImage(named: "detailed_data")!,
                                            title: "Lots of details",
                                            description: "WeatherCatch provides users with lots of details about todays weather",
                                            pageIcon: UIImage(named: "detailed_data_simple")!.withAlignmentRectInsets(UIEdgeInsets(top: -10, left: -10, bottom: -10, right: -10)),
                                            color: UIColor(red: 0.96, green: 0.62, blue: 0.14, alpha: 1),
                                            titleColor: .white,
                                            descriptionColor: .white,
                                            titleFont: .boldSystemFont(ofSize: 16),
                                            descriptionFont: .systemFont(ofSize: 14)))
        itemArray.append(OnboardingItemInfo(informationImage: UIImage(named: "day_save")!,
                                            title: "Save the days",
                                            description: "Take advantage of being able to save any day to be able in future see what weather was that day you saved",
                                            pageIcon: UIImage(named: "day_save_simple")!.withAlignmentRectInsets(UIEdgeInsets(top: -10, left: -10, bottom: -10, right: -10)),
                                            color: UIColor(red: 0.16, green: 0.76, blue: 0.82, alpha: 1),
                                            titleColor: .white,
                                            descriptionColor: .white,
                                            titleFont: .boldSystemFont(ofSize: 16),
                                            descriptionFont: .systemFont(ofSize: 14)))
    }
    
}
