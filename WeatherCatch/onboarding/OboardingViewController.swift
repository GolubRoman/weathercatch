//
//  ViewController.swift
//  WeatherCatch
//
//  Created by Roman Holub on 9/30/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import UIKit
import paper_onboarding
import RxSwift
import RxCocoa
import RxGesture

class OnboardingViewController: UIViewController, PaperOnboardingDataSource, PaperOnboardingDelegate {

    @IBOutlet weak var letsgoButton: UIButton!
    @IBOutlet weak var onboardingView: PaperOnboarding!
    private var disposeBag = DisposeBag()
    private var viewModel = OnboardingViewModel()
    private var wasScreenShown = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.insetsLayoutMarginsFromSafeArea = false
        self.initViews()
    }
    
    private func initViews() {
        onboardingView.dataSource = self
        onboardingView.delegate = self
        processLetsgoButtonVisibility(index: 0)
        letsgoButton.rx.tap
            .bind { self.performSegue(withIdentifier: "onboardingToCityListSegue", sender: self) }
            .disposed(by: disposeBag)
    }
    
    private func goToNextScreen() {
        self.performSegue(withIdentifier: "onboardingToCityListSegue", sender: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        if !wasScreenShown {
            viewModel.observeIfWeAlreadyHaveCities()?
                .observeOn(MainScheduler.instance)
                .subscribe(onSuccess: { haveCities in
                    if haveCities { self.goToNextScreen() }
                }, onError: { error in
                    print("Error: ", error)
                }).disposed(by: disposeBag)
            wasScreenShown = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func onboardingItemsCount() -> Int {
        return viewModel.getOnboardingArray().count
    }
    
    func onboardingItem(at index: Int) -> OnboardingItemInfo {
        return viewModel.getOnboardingArray()[index]
    }
    
    func onboardingWillTransitonToIndex(_ index: Int) {
        processLetsgoButtonVisibility(index: index)
    }
    
    private func processLetsgoButtonVisibility(index: Int) {
        letsgoButton.isHidden = index == viewModel.getOnboardingArray().count - 1 ? false : true
    }
    
}

