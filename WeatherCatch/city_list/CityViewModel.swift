//
//  CityViewModel.swift
//  WeatherCatch
//
//  Created by Roman Holub on 10/3/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import RxSwift

class CityViewModel {
    
    var cityInteractor: CityInteractor?
    var weatherInteractor: WeatherInteractor?

    init() {
        cityInteractor = (UIApplication.shared.delegate as! AppDelegate).interactorComponent.provideCityInteractor()
        weatherInteractor = (UIApplication.shared.delegate as! AppDelegate).interactorComponent.provideWeatherInteractor()
    }
    
    func observeIfWeAlreadyHaveCities() -> Single<Bool>? {
        return cityInteractor?.observeSavedCities()?
            .map { cities in cities.count >= 1 }
    }
    
    func observeCities(location: String) -> Maybe<Array<City>>? {
        return cityInteractor?.observeCities(location: location)?
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
    }
    
    func saveChosenCity(city: City?) -> Completable? {
        return cityInteractor?.saveChosenCity(city: city)
    }
    
}
