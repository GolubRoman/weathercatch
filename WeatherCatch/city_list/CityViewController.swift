//
//  ViewController.swift
//  WeatherCatch
//
//  Created by Roman Holub on 9/27/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import UIKit
import Lottie
import RxSwift
import RxCocoa
import RxGesture
import RxAlamofire
import Alamofire

class CityViewController: WholeScreenViewController {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var emptyListTextView: UILabel!
    @IBOutlet weak var emptyListImageView: UIImageView!
    @IBOutlet weak var titleImage: UIImageView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var cityCollectionView: UICollectionView!
    @IBOutlet weak var progressBar: AnimationView!
    let viewModel = CityViewModel()
    let disposeBag = DisposeBag()
    private var wasScreenShown = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !wasScreenShown {
            viewModel.observeIfWeAlreadyHaveCities()?
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { haveCities in
                if haveCities { self.goToNextScreen() }
            }, onError: { error in
                print("Error: ", error)
            }).disposed(by: disposeBag)
            wasScreenShown = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initViews()
    }
    
    private func goToNextScreen() {
        self.performSegue(withIdentifier: "cityListToMainSegue", sender: nil)
    }
    
    private func initViews() {
        initCollectionView()
        initSearchBar()
        showProgressBar()
        processDropDownKeyboard()
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    private func processDropDownKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    private func initSearchBar() {
        processSearchBarEvents()
        searchBar.searchBarStyle = .minimal
        if let textfield = searchBar.value(forKey: "searchField") as? UITextField {
            textfield.layer.backgroundColor = UIColor.init(red: 180, green: 180, blue: 180, alpha: 1).cgColor
            textfield.layer.shadowColor = UIColor.black.cgColor
            textfield.layer.shadowRadius = 2
            textfield.layer.shadowOpacity = 0.1
            textfield.layer.shadowOffset = CGSize(width: 4, height: 4)
            textfield.layer.masksToBounds = false
            textfield.layer.cornerRadius = 12
            textfield.textColor = UIColor.darkGray
        }
        
    }
    
    private func initCollectionView() {
        cityCollectionView.dataSource = nil
        setCollectionViewCellSize()
        processCitiesList(location: "Kiev")
        processCollectionViewCellTap()
        emptyListImageView.alpha = 0
        emptyListTextView.alpha = 0
        cityCollectionView.alpha = 0
        cityCollectionView.clipsToBounds = true
        cityCollectionView.layer.cornerRadius = 10
        cityCollectionView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
    }
    
    private func processCitiesList(location: String) {
        cityCollectionView.dataSource = nil
        viewModel.observeCities(location: location)?
            .delaySubscription(DispatchTimeInterval.seconds(1), scheduler: MainScheduler.instance)
            .flatMap { self.updateUiAccordingToCities(cities: $0) }
            .asObservable().bind(to: cityCollectionView.rx
                .items(cellIdentifier: "cityViewCell", cellType: CityViewCell.self)){ row, data, cell in
                    self.bindCityCell(row: row, data: data, cell: cell)
            }.disposed(by: disposeBag)
    }
    
    private func bindCityCell(row: Int, data: City, cell: CityViewCell) {
        cell.data = data
        cell.initImage(row: row)
    }
    
    private func updateUiAccordingToCities(cities: Array<City>) -> Maybe<Array<City>> {
        hideProgressBar()
        if (cities.isEmpty) { showEmptyListView() }
        else { self.cityCollectionView.fadeAnimation(duration: 1, initialAlpha: 0, finalAlpha: 1) }
        return Maybe.just(cities)
    }
    
    private func processSearchBarEvents() {
        searchBar.rx.searchButtonClicked
            .map { _ in
                self.hideEmptyListView()
                self.cityCollectionView.fadeAnimation(duration: 1, initialAlpha: 1, finalAlpha: 0)
                self.showProgressBar()
            }.delay(DispatchTimeInterval.seconds(1), scheduler: MainScheduler.instance)
            .subscribe({ _ in
                if let queryString = self.searchBar.text {
                    self.processCitiesList(location: queryString)
                }
            }).disposed(by: disposeBag)
    }
    
    private func processCollectionViewCellTap() {
        cityCollectionView.rx.modelSelected(City.self)
            .subscribe({ city in self.onTapCell(city: city.element) })
            .disposed(by: disposeBag)
    }
    
    private func onTapCell(city: City?) {
        showFetchCityProgressBar()
        viewModel.saveChosenCity(city: city)?
            .delaySubscription(DispatchTimeInterval.seconds(2), scheduler: ConcurrentDispatchQueueScheduler(qos: .background))
            .observeOn(MainScheduler.instance)
            .subscribe(onCompleted: {
                self.performSegue(withIdentifier: "cityListToMainSegue", sender: nil)
                self.hideFetchCityProgressBar()
            }, onError: { error in
                print("Error: ", error)
            }).disposed(by: disposeBag)
    }
    
    private func setCollectionViewCellSize() {
        let width = (cityCollectionView.frame.size.width) / 2 - 20
        let height = (cityCollectionView.frame.size.height) / 2 - 20
        let layout = cityCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: width, height: height)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 10
    }
    
}

