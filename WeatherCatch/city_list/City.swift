//
//  City.swift
//  WeatherCatch
//
//  Created by Roman Holub on 10/3/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation

class City {
    var name: String = ""
    var lat: String = ""
    var lon: String = ""
    
    init(name: String, lat: String, lon: String) {
        self.name = name
        self.lat = lat
        self.lon = lon
    }
    
}
