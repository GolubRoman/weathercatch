//
//  CityViewCell.swift
//  WeatherCatch
//
//  Created by Roman Holub on 10/3/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import UIKit

class CityViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var bottomCardView: UIView!
    
    var data: City? {
        didSet {
            guard let data = data else {
                return
            }
            initCell(city: data)
        }
    }
    
    func initImage(row: Int) {
        imageView.image = UIImage(named: "city_stub_" + String(row % 8))
    }
    
    func initCell(city: City) {
        cityName.text = city.name
        roundCorners()
    }
    
    private func roundCorners() {
        bottomCardView.clipsToBounds = true
        bottomCardView.layer.cornerRadius = 10
        bottomCardView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 10
    }
}
