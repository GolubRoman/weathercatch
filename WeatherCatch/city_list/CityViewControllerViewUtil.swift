//
//  CityViewControllerViewUtil.swift
//  WeatherCatch
//
//  Created by Roman Holub on 10/16/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import Lottie

extension CityViewController {
    
    func hideEmptyListView() {
        emptyListTextView.fadeAnimation(duration: 1, initialAlpha: 1, finalAlpha: 0)
        emptyListImageView.fadeAnimation(duration: 1, initialAlpha: 1, finalAlpha: 0)
    }
    
    func showEmptyListView() {
        emptyListTextView.fadeAnimation(duration: 1, initialAlpha: 0, finalAlpha: 1)
        emptyListImageView.fadeAnimation(duration: 1, initialAlpha: 0, finalAlpha: 1)
    }
    
    func showProgressBar(){
        progressBar.alpha = 0
        progressBar.fadeAnimation(duration: 1, initialAlpha: 0, finalAlpha: 1)
        progressBar.animation = Animation.named("jumping_marker_animation")
        progressBar.loopMode = LottieLoopMode.loop
        progressBar.play()
    }
    
    func hideProgressBar() {
        progressBar.fadeAnimation(duration: 1, initialAlpha: 1, finalAlpha: 0)
        executeOperationWithDelay(millisDelay: 1, disposeBag: disposeBag) {
            self.progressBar.stop()
        }
    }
    
    func showFetchCityProgressBar(){
        let duration = 0.5
        mainView.blurInView(duration: duration)
        
        let fetchCityProgressBar = AnimationView()
        fetchCityProgressBar.tag = 1000
        mainView.addSubview(fetchCityProgressBar)
        fetchCityProgressBar.scaleToFillParent(parent: mainView)
        fetchCityProgressBar.alpha = 0
        fetchCityProgressBar.fadeAnimation(duration: duration, initialAlpha: 0, finalAlpha: 1)
        fetchCityProgressBar.animation = Animation.named("fetch_city_animation")
        fetchCityProgressBar.loopMode = LottieLoopMode.loop
        fetchCityProgressBar.play()
        
        addFetchCityProgressBarLabel(text: "Reaching to your city", progressBarView: fetchCityProgressBar)
    }
    
    private func addFetchCityProgressBarLabel(text: String, progressBarView: UIView) {
        let progressBarLabel = UILabel()
        progressBarLabel.text = text
        progressBarLabel.textColor = UIColor.white
        progressBarLabel.font = UIFont.boldSystemFont(ofSize: 22.0)
        progressBarLabel.textAlignment = NSTextAlignment.center
        progressBarLabel.translatesAutoresizingMaskIntoConstraints = false
        progressBarLabel.tag = 1001
        
        mainView.addSubview(progressBarLabel)
        
        NSLayoutConstraint.activate(
            [progressBarLabel.leadingAnchor.constraint(equalTo: progressBarView.leadingAnchor),
                progressBarLabel.trailingAnchor.constraint(equalTo: progressBarView.trailingAnchor),
                progressBarLabel.centerYAnchor.constraint(equalTo: mainView.centerYAnchor, constant: 100)])
    }
    
    func hideFetchCityProgressBar() {
        let duration = 0.5
        for subview in mainView.subviews {
            if subview is AnimationView && subview.tag == 1000 {
                subview.fadeAnimation(duration: duration, initialAlpha: 1, finalAlpha: 0)
                mainView.blurOutView(duration: duration)
                executeOperationWithDelay(millisDelay: duration, disposeBag: disposeBag) {
                    (subview as! AnimationView).stop()
                }
            }
            if subview is UILabel && subview.tag == 1001 {
                subview.fadeAnimation(duration: duration, initialAlpha: 1, finalAlpha: 0)
                executeOperationWithDelay(millisDelay: duration, disposeBag: disposeBag) {
                    subview.removeFromSuperview()
                }
            }
        }
    }
    
}
