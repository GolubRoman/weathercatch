//
//  DbComponent.swift
//  WeatherCatch
//
//  Created by Roman Holub on 12/18/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import Swinject
import Realm
import RealmSwift
import RxSwift

class DbComponent: DiComponent {

    private let isDebugBuild: Bool
    private let container = Container()

    init(isDebugBuild: Bool) {
        self.isDebugBuild = isDebugBuild
    }
    
    func provideCityDao() -> CityDao? {
        return container.resolve(CityDao.self)
    }
    
    func setup() {
        registerCityDao()
    }
    
    private func registerCityDao() {
        container.register(CityDao.self) { _ in return CityDao() }
    }
}
