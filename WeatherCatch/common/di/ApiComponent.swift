//
//  CitiesProvider.swift
//  WeatherCatch
//
//  Created by Roman Holub on 10/8/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import Swinject

class ApiComponent: DiComponent {
    
    private let isDebugBuild: Bool
    private let container = Container()
    
    init(isDebugBuild: Bool) {
        self.isDebugBuild = isDebugBuild
    }
    
    func provideCityProvider() -> CityProvider? {
        return container.resolve(CityProvider.self)
    }
    
    func provideWeatherProvider() -> WeatherProvider? {
        return container.resolve(WeatherProvider.self)
    }
    
    func setup() {
        registerCityProvider()
        registerWeatherProvider()
    }
    
    private func registerCityProvider() {
        container.register(CityProvider.self) { _ in
            var provider: CityProvider
            if (self.isDebugBuild) { provider = DebugCityProvider() }
            else { provider = RemoteCityProvider() }
            
            return provider
        }
    }
    
    private func registerWeatherProvider() {
        container.register(WeatherProvider.self) { _ in
            var provider: WeatherProvider
            if (self.isDebugBuild) { provider = DebugWeatherProvider() }
            else { provider = RemoteWeatherProvider() }
            
            return provider
        }
    }
    
}
