//
//  DiComponent.swift
//  WeatherCatch
//
//  Created by Roman Holub on 10/11/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation

public protocol DiComponent {
    
    func setup()
}
