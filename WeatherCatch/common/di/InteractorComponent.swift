//
//  InteractorComponent.swift
//  WeatherCatch
//
//  Created by Roman Holub on 10/11/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import Swinject

class InteractorComponent: DiComponent {

    private let isDebugBuild: Bool
    private let apiComponent: ApiComponent
    private let dbComponent: DbComponent
    private let container = Container()
    
    init(isDebugBuild: Bool) {
        self.isDebugBuild = isDebugBuild
        apiComponent = ApiComponent(isDebugBuild: self.isDebugBuild)
        dbComponent = DbComponent(isDebugBuild: self.isDebugBuild)
    }
    
    func setup() {
        apiComponent.setup()
        dbComponent.setup()
        registerCityInteractor()
        registerWeatherInteractor()
    }
    
    func provideCityInteractor() -> CityInteractor? {
        return container.resolve(CityInteractor.self)
    }
    
    func provideWeatherInteractor() -> WeatherInteractor? {
        return container.resolve(WeatherInteractor.self)
    }
    
    private func registerCityInteractor() {
        container.register(CityInteractor.self) { _ in return CityInteractor(cityProvider: self.apiComponent.provideCityProvider(), cityDao: self.dbComponent.provideCityDao()) }
    }
    
    private func registerWeatherInteractor() {
        container.register(WeatherInteractor.self) { _ in return WeatherInteractor(weatherProvider: self.apiComponent.provideWeatherProvider()) }
    }
    
}
