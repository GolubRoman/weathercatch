//
//  CityProvider.swift
//  WeatherCatch
//
//  Created by Roman Holub on 10/8/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import RxSwift

protocol CityProvider {
    
    func getCities(location: String) -> Observable<Array<CityDto>>
    
}
