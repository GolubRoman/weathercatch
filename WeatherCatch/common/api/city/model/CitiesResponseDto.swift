//
//  CitiesResponseDto.swift
//  WeatherCatch
//
//  Created by Roman Holub on 10/9/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import ObjectMapper

public class CitiesResponseDto: Mappable {
    var poweredBy: String = ""
    var results: Array<CityDto> = []
    
    required init?(map: Map) {}
    
    // Mappable
    func mapping(map: Map) {
        poweredBy <- map["poweredBy"]
        results <- map["lat"]
    }
    
}
