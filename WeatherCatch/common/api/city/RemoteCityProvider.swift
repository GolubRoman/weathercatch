//
//  RemoteCityProvider.swift
//  WeatherCatch
//
//  Created by Roman Holub on 10/8/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import RxAlamofire
import RxSwift
import ObjectMapper

class RemoteCityProvider: CityProvider {
    
    func getCities(location: String) -> Observable<Array<CityDto>> {
        let url = "https://devru-latitude-longitude-find-v1.p.rapidapi.com/latlon.php"
        let parameters = ["location": location as Any]
        let headers = ["x-rapidapi-host": "devru-latitude-longitude-find-v1.p.rapidapi.com",
                       "x-rapidapi-key": "b056417076msh5d6c2ff54d0aaf8p1554afjsnd79eae34ecd9"]
        return requestString(.get, url, parameters: parameters, headers: headers)
            .debug()
            .map { (r, json) in
                var data: CitiesResponseDto? = nil
                data = Mapper<CitiesResponseDto>().map(JSONString: json)
                return data != nil ? data!.results : []
            }
    }
    
}
