//
//  DebugCityProvider.swift
//  WeatherCatch
//
//  Created by Roman Holub on 10/8/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import RxSwift

class DebugCityProvider: CityProvider {
    
    func getCities(location: String) -> Observable<Array<CityDto>> {
        var cities = Array<CityDto>()
        
        if location != "empty" {
            cities.append(CityDto(name: "Kyiv", lat: "40.40", lon: "50.50"))
            cities.append(CityDto(name: "Kharkiv", lat: "40.40", lon: "50.50"))
            cities.append(CityDto(name: "Lviv", lat: "40.40", lon: "50.50"))
            cities.append(CityDto(name: "Odessa", lat: "40.40", lon: "50.50"))
            cities.append(CityDto(name: "Dnipropetrovsk", lat: "40.40", lon: "50.50"))
            cities.append(CityDto(name: "Chernovtsy", lat: "40.40", lon: "50.50"))
            cities.append(CityDto(name: "Kriviy Rih", lat: "40.40", lon: "50.50"))
        }
        
        return Observable.just(cities)
    }
    
}
