//
//  CitiesResponseDto.swift
//  WeatherCatch
//
//  Created by Roman Holub on 10/9/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import ObjectMapper

class CitiesResponseDto: Mappable {
    var poweredBy: String = ""
    var results: Array<CityDto> = []
    
    required init?(map: Map) {}
    
    init(poweredBy: String, results: Array<CityDto>) {
        self.poweredBy = poweredBy
        self.results = results
    }
    
    // Mappable
    func mapping(map: Map) {
        poweredBy <- map["powerdBy"]
        results <- map["Results"]
    }
    
}

class CityDto: Mappable {
    var name: String = ""
    var lat: String = ""
    var lon: String = ""
    
    required init?(map: Map) {}
    
    init(name: String, lat: String, lon: String) {
        self.name = name
        self.lat = lat
        self.lon = lon
    }
    
    // Mappable
    func mapping(map: Map) {
        name <- map["name"]
        lat <- map["lat"]
        lon <- map["lon"]
    }
    
    func toCity() -> City {
        return City(name: self.name, lat: self.lat, lon: self.lon)
    }
    
}
