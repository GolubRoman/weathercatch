//
//  RemoteWeatherProvider.swift
//  WeatherCatch
//
//  Created by Roman Holub on 10/22/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import RxAlamofire
import RxSwift
import ObjectMapper

class RemoteWeatherProvider: WeatherProvider {
    
    func getCityWeatherForToday(coordinates: CoordinatesDto) -> Maybe<WeatherResponseDto> {
        let url = "https://api.openweathermap.org/data/2.5/weather"
        let parameters = ["lat": coordinates.lat as Any, "lon": coordinates.lon as Any,
                          "appid": "14a277fa3f105f703ccd00e54c6442bb" as Any, "units": "metric" as Any]
        return requestString(.get, url, parameters: parameters)
            .debug()
            .asMaybe()
            .flatMap { response in
                Maybe<WeatherResponseDto>.create { maybe in
                    let data: WeatherResponseDto? = Mapper<WeatherResponseDto>().map(JSONString: response.1)
                    if data != nil {
                        maybe(.success(data!))
                    } else {
                        maybe(.completed)
                    }
                    return Disposables.create()
                }
            }
    }
    
}
