//
//  WeatherDto.swift
//  WeatherCatch
//
//  Created by Roman Holub on 10/21/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import ObjectMapper

class WeatherResponseDto: Mappable {
    var coord: CoordinatesDto?
    var weather: [WeatherDto]?
    var base: String?
    var main: MainDto?
    var wind: WindDto?
    var clouds: CloudsDto?
    var dt: Int?
    var sys: SysDto?
    var timezone, id: Int?
    var name: String?
    var cod: Int?
    
    required init?(map: Map) {}
    
    init(coord: CoordinatesDto?, weather: [WeatherDto]?,
         base: String?, main: MainDto?, wind: WindDto?,
         clouds: CloudsDto?, dt: Int?, sys: SysDto?,
         timezone: Int?, id: Int?, name: String?, cod: Int?) {
             self.coord = coord
             self.weather = weather
             self.base = base
             self.main = main
             self.wind = wind
             self.clouds = clouds
             self.dt = dt
             self.sys = sys
             self.timezone = timezone
             self.id = id
             self.name = name
             self.cod = cod
    }
    
    // Mappable
    func mapping(map: Map) {
        coord <- map["coord"]
        weather <- map["weather"]
        base <- map["base"]
        main <- map["main"]
        wind <- map["wind"]
        clouds <- map["clouds"]
        dt <- map["dt"]
        sys <- map["sys"]
        timezone <- map["timezone"]
        id <- map["id"]
        name <- map["name"]
        cod <- map["cod"]
    }
    
    func toWeather() -> Weather {
        return Weather(locationName: self.name,
                       weatherStatus: self.weather?.first?.main,
                       weatherDescription: self.weather?.first?.description,
                       icon: self.weather?.first?.icon,
                       lat: self.coord?.lat,
                       lon: self.coord?.lon,
                       temp: self.main?.temp,
                       tempMin: self.main?.tempMin,
                       tempMax: self.main?.tempMax,
                       windSpeed: self.wind?.speed,
                       windDegree: self.wind?.deg,
                       seaLevel: self.main?.seaLevel,
                       groundLevel: self.main?.groundLevel,
                       pressure: self.main?.pressure,
                       humidity: self.main?.humidity,
                       cloudiness: self.clouds?.all,
                       sunrise: self.sys?.sunrise,
                       sunset: self.sys?.sunset,
                       timezone: self.timezone)
    }
    
}

class CoordinatesDto: Mappable {
    var lat: Double?
    var lon: Double?
    
    required init?(map: Map) {}
    
    init(lat: Double?, lon: Double?) {
        self.lat = lat
        self.lon = lon
    }
    
    // Mappable
    func mapping(map: Map) {
        lat <- map["lat"]
        lon <- map["lon"]
    }
    
}

class CloudsDto: Mappable {
    var all: Int?
    
    required init?(map: Map) {}
    
    init(all: Int?) {
        self.all = all
    }
    
    // Mappable
    func mapping(map: Map) {
        all <- map["all"]
    }
}

class WeatherDto: Mappable {
    var id: Int?
    var main, description, icon: String?

    required init?(map: Map) {}
    
    init(id: Int?, main: String?, description: String?, icon: String?) {
        self.id = id
        self.main = main
        self.description = description
        self.icon = icon
    }
    
    // Mappable
    func mapping(map: Map) {
        id <- map["id"]
        main <- map["main"]
        description <- map["description"]
        icon <- map["icon"]
    }
}

class MainDto: Mappable {
    var temp: Double?
    var pressure, humidity, tempMin, tempMax: Int?
    var seaLevel, groundLevel: Double?

    required init?(map: Map) {}
    
    init(temp: Double?, pressure: Int?, humidity: Int?, tempMin: Int?, tempMax: Int?,
         seaLevel: Double?, groundLevel: Double?) {
        self.temp = temp
        self.pressure = pressure
        self.humidity = humidity
        self.tempMin = tempMin
        self.tempMax = tempMax
        self.seaLevel = seaLevel
        self.groundLevel = groundLevel
    }
    
    // Mappable
    func mapping(map: Map) {
        temp <- map["temp"]
        pressure <- map["pressure"]
        humidity <- map["humidity"]
        tempMin <- map["temp_min"]
        tempMax <- map["temp_max"]
        seaLevel <- map["sea_level"]
        groundLevel <- map["grnd_level"]
    }
}

class WindDto: Mappable {
    var speed: Double?
    var deg: Int?
    
    required init?(map: Map) {}
    
    init(speed: Double?, deg: Int?) {
        self.speed = speed
        self.deg = deg
    }
    
    // Mappable
    func mapping(map: Map) {
        speed <- map["speed"]
        deg <- map["deg"]
    }
}

class SysDto: Mappable {
    var type, id: Int?
    var country: String?
    var sunrise, sunset: Int64?
    
    required init?(map: Map) {}
    
    init(type: Int?, id: Int?, country: String?, sunrise: Int64?, sunset: Int64?) {
        self.type = type
        self.id = id
        self.country = country
        self.sunrise = sunrise
        self.sunset = sunset
    }
    
    // Mappable
    func mapping(map: Map) {
        type <- map["type"]
        id <- map["id"]
        country <- map["country"]
        sunrise <- map["sunrise"]
        sunset <- map["sunset"]
    }
}
