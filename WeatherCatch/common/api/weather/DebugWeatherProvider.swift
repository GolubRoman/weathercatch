//
//  DebugWeatherProvider.swift
//  WeatherCatch
//
//  Created by Roman Holub on 10/21/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import RxSwift

class DebugWeatherProvider: WeatherProvider {
    
    func getCityWeatherForToday(coordinates: CoordinatesDto) -> Maybe<WeatherResponseDto> {
        return Maybe.just(WeatherResponseDto(coord: CoordinatesDto(lat: 139, lon: 35),
                                                    weather: [WeatherDto(id: 800,
                                                                         main: "Clear",
                                                                         description: "clear sky",
                                                                         icon: "01d")],
                                                    base: "stations",
                                                    main: MainDto(temp: 1.92,
                                                                  pressure: 1009,
                                                                  humidity: 92,
                                                                  tempMin: -3,
                                                                  tempMax: 5,
                                                                  seaLevel: 1023.22,
                                                                  groundLevel: 1013.75),
                                                    wind: WindDto(speed: 0.47,
                                                                  deg: 108),
                                                    clouds: CloudsDto(all: 2),
                                                    dt: 1560350192,
                                                    sys: SysDto(type: 3,
                                                                id: 2019346,
                                                                country: "JP",
                                                                sunrise: Int64(1575870362),
                                                                sunset: Int64(1575899672)),
                                                    timezone: 32400,
                                                    id: 1851632,
                                                    name: "Kyiv",
                                                    cod: 200))
    }
    
}
