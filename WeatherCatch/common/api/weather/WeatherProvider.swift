//
//  WeatherProvider.swift
//  WeatherCatch
//
//  Created by Roman Holub on 10/21/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import RxSwift

protocol WeatherProvider {
    
    func getCityWeatherForToday(coordinates: CoordinatesDto) -> Maybe<WeatherResponseDto>
    
}
