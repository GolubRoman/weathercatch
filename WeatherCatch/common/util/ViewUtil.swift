//
//  ViewUtil.swift
//  WeatherCatch
//
//  Created by Roman Holub on 10/16/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

extension UIView {
    
    func fadeAnimation(duration: Double, initialAlpha: CGFloat, finalAlpha: CGFloat) {
        if (alpha == initialAlpha) {
            UIView.animate(withDuration: duration, animations: { self.alpha = finalAlpha })
        }
    }
    
    func blurInView(duration: Double) {
        blurInView(duration: duration, style: .dark, alpha: 1)
    }
    
    func blurInView(duration: Double, style: UIBlurEffect.Style, alpha: CGFloat) {
        let blurEffect = UIBlurEffect(style: style)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        addSubview(blurredEffectView)
        blurredEffectView.scaleToFillParent(parent: self)
        
        blurredEffectView.alpha = 0
        UIView.animate(withDuration: duration, delay: 0, options: .curveEaseInOut, animations: {
          blurredEffectView.alpha = alpha
        }, completion: nil)
    }
    
    func blurOutView(duration: Double) {
        for subview in subviews {
            if subview is UIVisualEffectView {
                subview.alpha = 1
                UIView.animate(withDuration: duration, delay: 0, options: .curveEaseInOut, animations: { subview.alpha = 0 },
                               completion: { (finished: Bool) in subview.removeFromSuperview() })
            }
        }
    }
    
    func scaleToFillParent(parent: UIView) {
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [leadingAnchor.constraint(equalTo: parent.leadingAnchor),
                trailingAnchor.constraint(equalTo: parent.trailingAnchor),
                widthAnchor.constraint(equalTo: parent.widthAnchor),
                heightAnchor.constraint(equalTo: parent.heightAnchor)]
        )
    }
    
}
