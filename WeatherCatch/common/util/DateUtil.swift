//
//  DateExtension.swift
//  WeatherCatch
//
//  Created by Roman Holub on 12/9/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation

class DateUtil {
    
    static func getMillisBeginningOfTheDay() -> Int64 {
        var calendar = NSCalendar.current
        calendar.timeZone = NSTimeZone.local
        return calendar.startOfDay(for: Date()).millisecondsSince1970
    }

    private static func getDateBeginningOfTheDayFromMillis(millis: Int64?) -> Date {
        return Date.init(timeIntervalSince1970: TimeInterval(millis ?? Int64(0) + getMillisBeginningOfTheDay()))
    }

    static func getStringTimeBeginningOfTheDayFromMillis(millis: Int64?) -> String {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "HH:mm"
        return dateFormatterPrint.string(from: getDateBeginningOfTheDayFromMillis(millis: millis))
    }
}
