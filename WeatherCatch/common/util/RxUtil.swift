//
//  RxUtil.swift
//  WeatherCatch
//
//  Created by Roman Holub on 10/16/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import RxSwift

func executeOperationWithDelay(millisDelay: Double, disposeBag: DisposeBag, closure: @escaping (() -> Void)) {
    Completable.create { completable in
        closure()
        completable(.completed)
        return Disposables.create()
    }.delaySubscription(DispatchTimeInterval.milliseconds(Int(millisDelay * 1000)), scheduler: MainScheduler.instance)
    .subscribe()
    .disposed(by: disposeBag)
}

func executeSingleBackgroundOperation<T>(closure: @escaping (() -> T)) -> Single<T> {
    return Single.create { single in
            do { single(.success(closure())) }
            catch { single(.error(error)) }
            return Disposables.create()
        }.subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
    .observeOn(MainScheduler.instance)
}
