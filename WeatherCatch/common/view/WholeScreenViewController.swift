//
//  WholeScreenViewController.swift
//  WeatherCatch
//
//  Created by Roman Holub on 10/16/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import UIKit

class WholeScreenViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.insetsLayoutMarginsFromSafeArea = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
}
