//
//  WeatherInteractor.swift
//  WeatherCatch
//
//  Created by Roman Holub on 10/21/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import RxSwift

class WeatherInteractor {
    
    private var weatherProvider: WeatherProvider?
    
    init(weatherProvider: WeatherProvider?) {
        self.weatherProvider = weatherProvider
    }
    
    func observeWeatherForCity(city: City) -> Maybe<Weather>? {
        return weatherProvider?.getCityWeatherForToday(coordinates: CoordinatesDto(lat: Double(city.lat), lon: Double(city.lon)))
            .map { weatherDto in weatherDto.toWeather() }
    }
    
}
