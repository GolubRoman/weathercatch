//
//  CityInteractor.swift
//  WeatherCatch
//
//  Created by Roman Holub on 10/11/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import RxSwift

class CityInteractor {
    
    private var cityProvider: CityProvider?
    private var cityDao: CityDao?
    
    init(cityProvider: CityProvider?, cityDao: CityDao?) {
        self.cityProvider = cityProvider
        self.cityDao = cityDao
    }
    
    func observeCities(location: String) -> Maybe<Array<City>>? {
        return cityProvider?.getCities(location: location)
            .map { cities in cities.map { $0.toCity() } }
            .asMaybe()
    }
    
    func saveChosenCity(city: City?) -> Completable? {
        return cityDao?.upsertCity(city: CityEntity.fromCity(city: city)).asCompletable()
    }
    
    func observeSavedCities() -> Single<Array<City>>? {
        return cityDao?.observeCities()
    }
    
}
