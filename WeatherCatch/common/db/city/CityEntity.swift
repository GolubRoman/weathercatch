//
//  CityEntity.swift
//  WeatherCatch
//
//  Created by Roman Holub on 12/16/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class CityEntity: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var lat: String = ""
    @objc dynamic var lon: String = ""
    
    static func create(name: String, lat: String, lon: String) -> CityEntity {
        let city = CityEntity()
        city.name = name
        city.lat = lat
        city.lon = lon
        return city
    }
    
    static func fromCity(city: City?) -> CityEntity {
        if let city = city {
            let cityEntity = CityEntity()
            cityEntity.name = city.name
            cityEntity.lat = city.lat
            cityEntity.lon = city.lon
            return cityEntity
        } else {
            return CityEntity.create(name: "empty", lat: "-1", lon: "-1")
        }
    }
    
    func toCity() -> City {
        return City(name: self.name, lat: self.lat, lon: self.lon)
    }
}
