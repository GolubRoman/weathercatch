//
//  CityDao.swift
//  WeatherCatch
//
//  Created by Roman Holub on 12/16/19.
//  Copyright © 2019 Roman Holub. All rights reserved.
//

import Foundation
import RealmSwift
import Realm
import RxSwift

class CityDao {
    
    func upsertCity(city: CityEntity) -> Single<City> {
        return executeSingleBackgroundOperation() {
            let realm = try! Realm()
            realm.refresh()
            let oldCity = realm.objects(CityEntity.self).filter("name == '\(city.name)'").first
            if oldCity != nil {
                try! realm.write {
                    oldCity?.name = city.name
                    oldCity?.lat = city.lat
                    oldCity?.lon = city.lon
                }
            } else {
                try! realm.write {
                    realm.add(city)
                }
            }
            return city.toCity()
        }
    }
    
    func observeCities() -> Single<Array<City>> {
        return executeSingleBackgroundOperation() {
            let realm = try! Realm()
            realm.refresh()
            return Array(realm.objects(CityEntity.self)).map { $0.toCity() }
        }
    }
}
